const { Given, When, Then } = require('cucumber');
const AutomationPage = require('../pages/automation.page');

Given('Acessar o site {string}', (endereco) => {
  AutomationPage.acessar(endereco);
});

When('Escolha um produto qualquer na loja', () => {
  AutomationPage.escolherProduto();
});

When('Adicione o produto escolhido ao carrinho', () => {
  AutomationPage.adicionarCarrinho();
});

When('Prossiga para o checkout', () => {
  AutomationPage.selecionarCheckout();
});

When('Valide se o produto foi corretamente adicionado ao carrinho e prossiga caso esteja tudo certo', () => {
  AutomationPage.validarProdutoCarrinho();
});

When('Realize o cadastro do cliente preenchendo todos os campos obrigatórios dos formulários', () => {
  AutomationPage.realizarCadastro();
});

When('Valide se o endereço está correto e prossiga', () => {
  AutomationPage.validarEndereco();
});
When('Aceite os termos de serviço e prossiga', () => {
  AutomationPage.aceitarTermos();
});

When('Valide o valor total da compra', () => {
  AutomationPage.validarValor();
});

When('Selecione um método de pagamento e prossiga', () => {
  AutomationPage.selecionarPagamento();
});

Then('Confirme a compra e valide se foi finalizada com sucesso', () => {
  AutomationPage.confirmarCompra();
});
