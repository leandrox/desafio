class AutomationMap {
  get liProduto() {
    return this.validarElemento('#homefeatured li');
  }

  get spanAdicionar() {
    return this.validarElemento('span=Add to cart');
  }
  get aProsseguir() {
    return this.validarElemento('a[title*=Proceed]');
  }

  get aQuickView() {
    return this.validarElemento('a.quick-view');
  }
  get spanMore() {
    return $('span=More');
  }
  get h1Nome() {
    return this.validarElemento('[itemprop=name]');
  }
  get spanValor() {
    return this.validarElemento('[itemprop=price]');
  }
  get pNome() {
    return this.validarElemento('.cart_description p');
  }
  get spanValorCarrinho() {
    return this.validarElemento('.cart_unit span');
  }
  get spanProsseguir() {
    return this.validarElemento('.cart_navigation span');
  }

  get inputEmail() {
    return this.validarElemento('#email_create');
  }
  get buttonCriar() {
    return this.validarElemento('#SubmitCreate');
  }

  get radioMasculino() {
    return this.validarElemento('#uniform-id_gender1');
  }

  get inputNome() {
    return this.validarElemento('#customer_firstname');
  }

  get inputSobrenome() {
    return this.validarElemento('#customer_lastname');
  }

  get inputSenha() {
    return this.validarElemento('#passwd');
  }

  get selDia() {
    return $('#days');
  }

  get selMes() {
    return $('#months');
  }

  get selAno() {
    return $('#years');
  }

  get inputNomeEnd() {
    return this.validarElemento('#firstname');
  }

  get inputSobrenomeEnd() {
    return this.validarElemento('#lastname');
  }

  get inputEndereco() {
    return this.validarElemento('#address1');
  }

  get inputCidade() {
    return this.validarElemento('#city');
  }

  get selEstado() {
    return $('#id_state');
  }
  get inputCEP() {
    return this.validarElemento('#postcode');
  }
  get inputTelefone() {
    return this.validarElemento('#phone_mobile');
  }
  get buttonRegistrar() {
    return this.validarElemento('#submitAccount');
  }
  validarEndereco(valor) {
    return this.validarElemento(`div*=${valor}`);
  }

  get checkboxTermos() {
    $('#cgv').waitForExist();
    return $('#cgv');
  }
  get aCheque() {
    return this.validarElemento('a.cheque');
  }
  get buttonConfirmar() {
    return this.validarElemento('span=I confirm my order');
  }

  validarElemento(valor) {
    browser.waitForVisible(valor);
    return $(valor);
  }
}

const automationMap = new AutomationMap();
module.exports = automationMap;
