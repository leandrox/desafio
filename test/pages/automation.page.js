const expect = require('chai').expect;
const AutomationMap = require('../objects/automation.map');

var nomeProduto;
var valorProduto;
var dadosPessoais = ['test1000'+ Math.round(Math.random() * 1000)+'@te.com', 'Fernando', 'Test', 'Teste', '1', '1', '1999'];
var dadosEndereco = ['Fernando', 'Test', 'Rua Teste, 2', 'San Pablo', '1', '23131', '55119213812273'];

class AutomationPage {
  acessar(endereco) {
    browser.url(endereco);
    browser.windowHandleMaximize();
  }

  escolherProduto() {
    AutomationMap.liProduto.click();
    if(AutomationMap.spanMore.isExisting()) AutomationMap.spanMore.click();
    nomeProduto = AutomationMap.h1Nome.getText();
    valorProduto = AutomationMap.spanValor.getText();
  }

  adicionarCarrinho() {
    AutomationMap.spanAdicionar.click();
  }

  selecionarCheckout() {
    AutomationMap.aProsseguir.click();
  }

  validarProdutoCarrinho() {
    expect(AutomationMap.pNome.getText()).to.equal(nomeProduto);
    expect(AutomationMap.spanValorCarrinho.getText()).to.equal(valorProduto);
    AutomationMap.spanProsseguir.click();
  }

  realizarCadastro() {
    AutomationMap.inputEmail.setValue(dadosPessoais[0]);
    AutomationMap.buttonCriar.click();
    AutomationMap.radioMasculino.click();
    AutomationMap.inputNome.setValue(dadosPessoais[1]);
    AutomationMap.inputSobrenome.setValue(dadosPessoais[2]);
    AutomationMap.inputSenha.setValue(dadosPessoais[3]);
    AutomationMap.selDia.selectByValue(dadosPessoais[4]);
    AutomationMap.selMes.selectByValue(dadosPessoais[5]);
    AutomationMap.selAno.selectByValue(dadosPessoais[6]);
    AutomationMap.inputNomeEnd.setValue(dadosEndereco[0]);
    AutomationMap.inputSobrenomeEnd.setValue(dadosEndereco[1]);
    AutomationMap.inputEndereco.setValue(dadosEndereco[2]);
    AutomationMap.inputCidade.setValue(dadosEndereco[3]);
    AutomationMap.selEstado.selectByValue(dadosEndereco[4]);
    AutomationMap.inputCEP.setValue(dadosEndereco[5]);
    AutomationMap.inputTelefone.setValue(dadosEndereco[6]);
    AutomationMap.buttonRegistrar.click();
  }

  validarEndereco() {
    dadosEndereco.forEach((dado) => {
      expect(AutomationMap.validarEndereco(dado).isVisible()).to.equal(true);
    });
    AutomationMap.spanProsseguir.click();
  }
  aceitarTermos() {
    AutomationMap.checkboxTermos.click();
    AutomationMap.spanProsseguir.click();
  }
  validarValor() {
    expect(AutomationMap.spanValorCarrinho.getText()).to.equal(valorProduto);
  }
  selecionarPagamento() {
    AutomationMap.aCheque.click();
  }
  confirmarCompra() {
    AutomationMap.buttonConfirmar.click();
    expect(browser.waitForExist('.alert-success')).to.equal(true);
  }
}

const automationPage = new AutomationPage();
module.exports = automationPage;
