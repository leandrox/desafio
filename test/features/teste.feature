Feature: Teste

    Scenario: Realizar uma compra com sucesso.
        Given Acessar o site "http://www.automationpractice.com"
        When Escolha um produto qualquer na loja
        When Adicione o produto escolhido ao carrinho
        When Prossiga para o checkout
        When Valide se o produto foi corretamente adicionado ao carrinho e prossiga caso esteja tudo certo
        When Realize o cadastro do cliente preenchendo todos os campos obrigatórios dos formulários
        When Valide se o endereço está correto e prossiga
        When Aceite os termos de serviço e prossiga
        When Valide o valor total da compra
        When Selecione um método de pagamento e prossiga
        Then Confirme a compra e valide se foi finalizada com sucesso