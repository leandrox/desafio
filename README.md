# Desafio técnico de automação de testes 

É um projeto de automação de teste usando Selenium WebdriverIO (Node.js usando W3C WebDriver protocol), cucumber seguindo Page Object Pattern.

## Pré-requisito

npm + [Node.js](https://nodejs.org/dist/v8.12.0/node-v8.12.0-x64.msi)

## Instalação

Executar o comando abaixo dentro do diretório raiz após clonar o projeto e assim instalar todas as dependências:

```sh
npm install
```


## Execução

Para executar os testes na sua máquina local, é necessário o seguinte comando:

```sh
npm test
```
